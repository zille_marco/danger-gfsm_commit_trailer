# frozen_string_literal: true

module Danger
  # Run each commit in the MR through the check.
  #
  # GFSM Commit Tralier will make sure that at least one commit
  # inside the MR has a 'CHANGELOG: ' trailer.
  #
  # @example Warn instead of fail
  #
  #          gfsm_commit_trailer.check(:warn: true)
  #
  # @see danger/danger
  class DangerGfsmCommitTrailer < Plugin
    ERROR_MESSAGE = "This MR has no commits with a CHANGELOG trailer."
    CHANGELOG_TRAILER_REGEX = /^(?<name>Changelog):\s*(?<category>.+)$/i.freeze

    # Checks the commits using the config passed by the user
    #
    # Passing in a hash which contains the following keys
    #
    # * `warn` - boolean value to tell whether or not eventual errors
    #            should be reported as warnings
    #
    # @param [Hash] config
    #
    # @return [void]
    def check(config = {})
      unless changelog_trailers?
        if config.key?(:warn) && config[:warn]
          messaging.warn ERROR_MESSAGE
        else
          messaging.fail ERROR_MESSAGE
        end
      end
    end

    private

    def changelog_trailers?
      git.commits.each do |commit|
        trailer = commit.message.match(CHANGELOG_TRAILER_REGEX)
        return true if trailer
      end

      false
    end
  end
end
